package ru.demo.managers;

import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.demo.model.to.CardTO;
import ru.demo.model.Card;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dmitri
 */
@Component(value = "cardManager")
public class CardManager {
    /**
     * The enum of the input CSV-format fields with theirs metadata to process
     */
    private enum DETAILS {
        BANK_NAME(0, "bank name", "^\\s*(.+?)\\s*$"),
        CARD_NUMBER(1, "bank card number", "^\\s*([0-9]{4}-[0-9]{4}-[0-9]{4})(-[0-9]{1,4})?(-[1-9]{1,3})?\\s*$"),
        EXPIRY_DATE(2, "card expiry date", "^\\s*([-a-zA-Z\\s0-9]+?)\\s*;?\\s*$");

        private int id;
        String description;
        private String regexp;

        DETAILS(int id, String description, String regexp) {
            this.id = id;
            this.description = description;
            this.regexp = regexp;
        }

        public int getId() {
            return id;
        }
        public String getDescription() {
            return description;
        }
        public String getRegexp() {
            return regexp;
        }
    }

    Logger logger = LoggerFactory.getLogger(getClass());

    public static SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");

    /**
     * Creating Card instance based on the manual input data
     * @param details The transport object to populate the card details manually.
     * Where <details.bankName> - the string with the Bank name (may contain even the delimiter comma);
     * <details.cardNumber> - the bank card number in the format XXXX-XXXX-...XX; may have length from 12 to 19 grouped by four digits (from 1234-5678-9012 to 1234-5678-9012-3456-789);
     * <details.expiryDate> - bank card expiry date
     * @return Successfully created instance of Card if input data validated, otherwise null.
     */
    public Card createCard(CardTO details) {
        String expiryDate = String.format("%s-%s", details.expiryMonth, details.expiryYear);
        String[] inputs = {details.bankName, details.cardNumber, expiryDate};
        return createCard(inputs);
    }

    /**
     * Parsing the input line and creating the Card instance when succeeded.
     * @param inputs The bank card details in following order:
     *  <bank name>,<card number>,<card expiry date>
     * Where <bank name> - the string with the Bank name (may contain even the delimiter comma);
     * <card number> - the bank card number in the format XXXX-XXXX-...XX; may have length from 12 to 19 grouped by four digits (from 1234-5678-9012 to 1234-5678-9012-3456-789);
     * <card expiry date> - bank card expiry date in format MMM-yyyy (e.g. Nov-2017)
     * @return The created instanced with parsed details if succeeded, otherwise null.
     */
    public Card createCard(String[] inputs) {
        Card res = null;
        Map<DETAILS, Object> detailValueMap = new HashMap<DETAILS, Object>();

        if (inputs.length != DETAILS.values().length) {
            String inputsToStr = Joiner.on(",").join(Arrays.asList(inputs));
            logger.warn("Cannot fill all details. Expected {} details, but actual has {}: '{}'",
                    new Object[]{DETAILS.values().length, inputs.length, inputsToStr});
            return res;
        }

        for (DETAILS detail : DETAILS.values()) {
            if (detail.getId() >= inputs.length) {
                String inputsToStr = Joiner.on(",").join(Arrays.asList(inputs));
                logger.warn("Cannot find the value {} (#{}) in the following input line: '{}'",
                        new Object[]{detail.getDescription(), detail.getId() + 1, inputsToStr});
                return res;
            }

            String input = inputs[detail.getId()];
            if (StringUtils.isEmpty(input) || !input.matches(detail.getRegexp())) {
                String inputsToStr = Joiner.on(",").join(Arrays.asList(inputs));
                logger.warn("Cannot parse {} csv-record value of '{}' in the following input line: '{}'",
                        new Object[]{detail.getDescription(), input, inputsToStr});
                return res;
            }

            if (!parseDetailValue(detailValueMap, detail, input, inputs)) {
                return res;
            }
        }

        String bankName = (String)detailValueMap.get(DETAILS.BANK_NAME);
        String cardNumber = (String)detailValueMap.get(DETAILS.CARD_NUMBER);
        Date expiryDate= (Date)detailValueMap.get(DETAILS.EXPIRY_DATE);

        //in case of any detail among DETAILS enum is left unset
        if (bankName == null || cardNumber == null || expiryDate == null) {
            String inputsToStr = Joiner.on(",").join(Arrays.asList(inputs));
            DETAILS failedDetail = bankName == null ? DETAILS.BANK_NAME :
                            cardNumber == null ? DETAILS.CARD_NUMBER : DETAILS.EXPIRY_DATE;
            logger.error("Cannot find the {} (expected as #{} detail) among the following input line: '{}'",
                    new Object[] {failedDetail.getDescription(), failedDetail.getId() + 1, inputsToStr});
            return res;
        }

        res = new Card(bankName, cardNumber, expiryDate);
        return res;
    }

    protected boolean parseDetailValue(Map<DETAILS, Object> detailValueMap, DETAILS detail, String input, String[] inputs) {
        boolean passed = true;
        Pattern pattern = Pattern.compile(detail.getRegexp());
        Matcher matcher = pattern.matcher(input);
        if (!matcher.matches()) {
            String inputsToStr = Joiner.on(",").join(Arrays.asList(inputs));
            logger.error("Cannot match the matcher '{}' with the input '{}' among the following input line: '{}'",
                    new Object[] {detail.getRegexp(), input, inputsToStr});
            return !passed;
        }

        //appending all matched groups through the result
        StringBuilder builder = new StringBuilder();
        int i = 1, count = matcher.groupCount();
        do {
            String group = matcher.group(i);
            if (group != null) {
                builder.append(group);
            }
            i++;
        } while (i <= count);
        String value = builder.toString();

        if (detail.getId() != DETAILS.EXPIRY_DATE.getId()) {
            detailValueMap.put(detail, value);
        } else {
            //in case of expiry date detail, need to get a Date instance
            try {
                Date date = sdf.parse(value);
                detailValueMap.put(detail, date);
            } catch (ParseException e) {
                String inputsToStr = Joiner.on(",").join(Arrays.asList(inputs));
                logger.warn("Cannot parse the expiry date value '{}' (expected format 'MMM-yyyy') with the input value of '{}' among the following input line: '{}'",
                        new Object[] {value, input, inputsToStr});
                return !passed;
            }
        }
        return passed;
    }


}
