package ru.demo.services;

import ru.demo.model.Card;

import java.io.InputStream;
import java.util.List;

/**
 * Created by dmitri
 */
public interface InputDocumentParser {
    /**
     * Parsing the input stream of CSV-formated string.
     * @param input The bank card details in following format:
     *  <bank name>,<card number>,<card expiry date>
     * Where <bank name> - the string with the Bank name (may contain even the delimiter comma);
     * <card number> - the bank card number in the format XXXX-XXXX-...XX; may have length from 12 to 19 grouped by four digits (from 1234-5678-9012 to 1234-5678-9012-3456-789);
     * <card expiry date> - bank card expiry date in format MMM-yyyy (e.g. Nov-2017)
     * @return The list of created instanced with parsed details if succeeded, otherwise empty list.
     */
    List<Card> parse(InputStream input);
}
