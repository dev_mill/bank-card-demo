package ru.demo.services;

import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.demo.managers.CardManager;
import ru.demo.model.Card;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmitri
 */
@Component(value = "inputDocumentParser")
public class InputDocumentParserImpl implements InputDocumentParser {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CardManager cardManager;

    /**
     * @param input The bank card details in following format:
     *  <bank name>,<card number>,<card expiry date>
     * Where <bank name> - the string with the Bank name (may contain even the delimiter comma);
     * <card number> - the bank card number in the format XXXX-XXXX-...XX; may have length from 12 to 19 grouped by four digits (from 1234-5678-9012 to 1234-5678-9012-3456-789);
     * <card expiry date> - bank card expiry date in format MMM-yyyy (e.g. Nov-2017)
     * @return
     */
    public List<Card> parse(InputStream input) {
        List<Card> res = new ArrayList<>();

        InputStreamReader streamReader = new InputStreamReader(input);
        CSVReader reader = new CSVReader(streamReader);
        String[] nextLine;
        try {
            while ((nextLine = reader.readNext()) != null) {
                // nextLine[] is an array of values from the line
                Card card = cardManager.createCard(nextLine);
                if (card != null) {
                    res.add(card);
                }
            }

            reader.close();
            streamReader.close();
        } catch (IOException e) {
            logger.error("Cannot parse csv input stream: " + e.getMessage(), e);
        }

        return res;
    }
}
