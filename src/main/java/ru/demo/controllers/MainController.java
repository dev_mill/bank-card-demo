package ru.demo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.demo.managers.CardManager;
import ru.demo.model.Card;
import ru.demo.model.to.CardTO;
import ru.demo.services.InputDocumentParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

@Controller
@Scope(value = "session")
public class MainController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    CardManager cardManager;
    @Autowired
    InputDocumentParser parser;

    Set<Card> cards = new ConcurrentSkipListSet<>();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView mav = new ModelAndView("main");
        mav.addObject("cards", cards);
        CardTO cardTO = new CardTO();
        mav.addObject("cardTO", cardTO);
        return mav;
    }

    @RequestMapping(value = "/addCardDetail", method = RequestMethod.POST)
    public String addCardDetail(@ModelAttribute("cardTO") CardTO details,
                                BindingResult bindingResult, Model model) {
        Card card = cardManager.createCard(details);

        ModelAndView mav = new ModelAndView();
        mav.addAllObjects(model.asMap());
        if (card == null) {
            model.addAttribute("error", "Cannot create the card with given details.");
        } else {
            cards.add(card);
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/clearAll", method = RequestMethod.GET)
    public String clearAll(ModelMap model) {
        cards.clear();
        return "redirect:/";
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public
    @ResponseBody
    String uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
        long fileSize = file.getSize();
        String fileContentType = file.getContentType();
        int indexOfType = fileContentType.indexOf("text");
        int indexOfDelim = fileContentType.indexOf("/");

        String res;

        if (file.isEmpty()) {
            logger.warn("Cannot upload file '{}' size of '{}B': it's empty.", fileName, fileSize);
            res = String.format("You file '%s' is empty. Try another one.", fileName);
        } else if (indexOfType == -1 || indexOfType > indexOfDelim) {
            logger.warn("Cannot upload file '{}' size of '{}B': incorrect content type '{}'.",
                    new Object[]{fileName, fileSize, fileContentType});
            res = String.format("You file '%s' must be of plain text type. Try another one.", fileName);
        } else {
            try {
                InputStream inputStream = file.getInputStream();
                List<Card> list = parser.parse(inputStream);
                int added = 0;
                for (Iterator<Card> iterator = list.iterator(); iterator.hasNext(); ) {
                    Card card = iterator.next();
                    if (!cards.contains(card)) {
                        cards.add(card);
                        added++;
                    }
                }
                res = String.format("Your file '%s' was uploaded successfully. Parsed '%s' records" + ", among which only '%s' were new/unique and added.",
                        fileName, list.size(), added);
                inputStream.close();
            } catch (IOException e) {
                logger.error(String.format("Cannot upload file '%s' size of '%sB'.", fileName, fileSize), e);
                res = "Cannot create the card record with the given details.";
            }
        }

        return res;
    }
}