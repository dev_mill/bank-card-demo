package ru.demo.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dmitri
 */
public class Card implements Comparable<Card> {
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    private static SimpleDateFormat sdfView = new SimpleDateFormat("MMM-yyyy");

    private Logger logger = LoggerFactory.getLogger(getClass());

    private String bank;
    private String number;
    private Date expiryDate;

    public Card() {}

    public Card(String bank, String number, Date expiryDate) {
        this.bank = bank;
        this.number = number;
        this.expiryDate = expiryDate;
    }

    public String getBank() {
        return bank;
    }
    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
    public String getNumberMasked() {
        String mask = new StringBuilder(number.substring(0,4))
                .append(number.substring(4).replaceAll("[^-]", "x")).toString();
        return mask;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
    public void setExpiryDate(String input) throws ParseException {
        expiryDate = sdfView.parse(input);
    }

    /**
     * Comparing the Cards first by expiry date (descending), then bank name and card number.
     * @param o The other instance to compare to.
     * @return 1 if this is previous than o, 0 once they are equal, otherwise -1.
     */
    @Override
    public int compareTo(Card o) {
        int res = 0;

        if (this == o) {
            return res;
        }

        res = this.expiryDate.compareTo(o.expiryDate);
        if (res != 0) {
            return -1 * res;
        }

        res = this.bank.compareTo(o.bank);
        if (res != 0) {
            return res;
        }

        res = this.number.compareTo(o.number);
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Card)) {
            return false;
        }

        return compareTo((Card)o) == 0;
    }

    @Override
    public int hashCode() {
        int result = bank.hashCode();
        result = 31 * result + number.hashCode();
        result = 31 * result + expiryDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "bank='" + bank + '\'' +
                ", number='" + number + '\'' +
                ", expiryDate=" + sdf.format(expiryDate) +
                "}";
    }
}
