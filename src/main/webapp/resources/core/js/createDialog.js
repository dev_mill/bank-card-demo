$(document).ready(function () {
    $environment.cardNumberRegex = /^\s*([0-9]{4}-[0-9]{4}-[0-9]{4})(-[0-9]{1,4})?(-[1-9]{1,3})?\s*$/;
    $environment.yearRegex = /^((19[789]|20[012])[0-9]|2030)$/;      //min="1970", max="2030"
    $environment.bankName = $("#bankName");
    $environment.cardNumber = $("#cardNumber");
    $environment.expiryMonth = $("#expiryMonth");
    $environment.expiryYear = $("#expiryYear");
    $environment.allFields = $([])
        .add(this.bankName).add(this.cardNumber)
        .add(this.expiryMonth).add(this.expiryYear);
    $environment.createForm = $("#dialog-form").find("form")[0];
    $environment.createDialog = $("#dialog-form").dialog({
        autoOpen: false,
        width: 450,
        modal: true,
        buttons: [
            {
                text: "Add details",
                click: function () {
                    var createFormSubmitBtn = $($environment.createDialog).parent().find("button:submit");
                    createFormSubmitBtn.attr('disabled', 'disabled');

                    if (addCardDetail()) {
                        $environment.createForm.submit();
                        closeCreateDialog();
                    }

                    createFormSubmitBtn.removeAttr('disabled', 'disabled');
                },
                type: "submit"
            },
            {
                text: "Cancel",
                click: function () {
                    closeCreateDialog();
                }
            }
        ],
        close: function () {
            $environment.createForm.reset();
            $environment.allFields.removeClass("ui-state-error");
        }
    });

    var monthValues = [];
    var list = $("#monthList").children();
    $.each(list, function (i, v) {
        monthValues.push($(v).val());
    });
    $environment.monthRegex = new RegExp("^(" + monthValues.join("|") + ")$");

    $("#create-card-detail-open").button().on("click", function () {
        openCreateDialog();
        return false;
    });

    function openCreateDialog() {
        $environment.createDialog.dialog("open");
        if ($environment.createDialog.hasClass('hidden')) {
            $environment.createDialog.removeClass('hidden');
        }
    }

    function closeCreateDialog() {
        if (!$environment.createDialog.hasClass('hidden')) {
            $environment.createDialog.addClass('hidden');
        }
        $environment.createDialog.dialog("close");
    }
});