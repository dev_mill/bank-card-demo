$(document).ready(function() {
    addCardDetail = function(event) {
        var valid = true;
        $environment.allFields.removeClass("ui-state-error");

        valid = valid && checkLength($environment.bankName, "the bank name", 1, 250);
        valid = valid && checkLength($environment.cardNumber, "the card number", 14, 23);
        valid = valid && checkRegexp($environment.cardNumber, $environment.cardNumberRegex, "The integer number with length from 12 to 19, grouped by 4 signs and separated by dash");
        valid = valid && checkRegexp($environment.expiryMonth, $environment.monthRegex, "You should choose a month from the list only.");
        valid = valid && checkRegexp($environment.expiryYear, $environment.yearRegex, "The bank card expiry year must be in the interval between 1970 and 2030");

        if (!valid) {
            if (!!event) {
                event.preventDefault()
            }
        }
        return valid;
    };

    function updateTips(t) {
        $environment.tips
            .text(t)
            .addClass("ui-state-highlight");
        setTimeout(function () {
            $environment.tips.removeClass("ui-state-highlight", 1500);
        }, 500);
    }

    function checkLength(o, n, min, max) {
        var valid = true;
        if (o.val().length > max || o.val().length < min) {
            o.addClass("ui-state-error");
            updateTips("Length of " + n + " must be between " + min + " and " + max + ".");
            return !valid;
        } else {
            return valid;
        }
    }

    function checkRegexp(o, regexp, n) {
        if (!(regexp.test(o.val()))) {
            o.addClass("ui-state-error");
            updateTips(n);
            return false;
        } else {
            return true;
        }
    }
});