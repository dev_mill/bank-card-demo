$(document).ready(function () {
    $environment.uploadDialog = $("#upload-file-container").dialog({
        autoOpen: false,
        width: 350,
        modal: true,
        buttons: [
            {
                text: "Upload",
                click: function () {
                    $($environment.uploadDialog).parent().find("button:submit").attr('disabled', 'disabled');
                    uploadFile();
                },
                type: "submit"
            },
            {
                text: "Cancel",
                click: function () {
                    closeUploadDialog();
                }
            }
        ],
        close: function () {
        }
    });

    $("#upload-file-open").button().on("click", function () {
        openUploadDialog();
        return false;
    });

    $environment.uploadDialog.find(':file').change(function () {
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
        if (!!console) {
            console.debug("selected file '" + name + "', size of " + size + " and type of '" + type + "'B.");
        }

        if (!type.match(/text.*/)) {
            alert("You need to select a CSV file.");
            return false;
        }

        if (size > 52428800) {
            alert("Maximum size of a CSV file is only 50MB (for a demo version).");
            return false;
        }
    });
});

function uploadFile() {
    var formData = new FormData($environment.uploadDialog.find("form")[0]);
    $.ajax({
        url: location.pathname + 'uploadFile',
        type: 'POST',
        beforeSend: function(jqXHR, settings) {
            $environment.uploadInput
        },
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
            }
            return myXhr;
        },
        success: function(data, textStatus, jqXHR) {
            closeUploadDialog();
            if (textStatus === "success") {
                alert(data);
            } else {
                alert("Couldn't upload your file: " + data);
            }
            location.href = location.pathname;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var status = (!!textStatus) ? textStatus : "error";
            var message = "Couldn't complete file uploading due to " + status + "(s). ";
            if (!!errorThrown) {
                message += errorThrown + ". ";
            }
            if (!!jqXHR.statusText) {
                message += "Status text: " + jqXHR.statusText + ".";
            }
            alert(message);
            window.location = location.pathname;
        },
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    });
}

function progressHandlingFunction(e) {
    if (e.lengthComputable) {
        $('progress').attr({value: e.loaded, max: e.total});
    }
}

function openUploadDialog() {
    $environment.uploadDialog.dialog("open");
    if ($environment.uploadDialog.hasClass('hidden')) {
        $environment.uploadDialog.removeClass('hidden');
    }
}
function closeUploadDialog() {
    if (!$environment.uploadDialog.hasClass('hidden')) {
        $environment.uploadDialog.addClass('hidden');
    }
    $environment.uploadDialog.dialog("close");
}
