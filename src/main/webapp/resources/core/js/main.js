$(document).ready(function () {
    $("#clear-all").button().on('click', function(e) {
        if (confirm("Do you really want to clear the card details list?")) {
            window.location = $(this).attr('href');
        }
    });
});
