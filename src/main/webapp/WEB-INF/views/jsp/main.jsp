<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bank card demo</title>

    <spring:url value="/resources/core/css/main.css" var="coreCss"/>
    <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss"/>
    <spring:url value="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" var="jQueryUiCss"/>

    <spring:url value="/resources/core/js/jquery-2.1.4.min.js" var="jQueryJS"/>
    <spring:url value="/resources/core/js/jquery-ui.min.js" var="jQueryUiJS"/>
    <spring:url value="/resources/core/js/environment.js" var="environmentJS"/>
    <spring:url value="/resources/core/js/createDialogValidation.js" var="createDialogValidationJS"/>
    <spring:url value="/resources/core/js/createDialog.js" var="createDialogJS"/>
    <spring:url value="/resources/core/js/uploadDialog.js" var="uploadDialogJS"/>
    <spring:url value="/resources/core/js/main.js" var="mainJs"/>

    <link href="${bootstrapCss}" rel="stylesheet"/>
    <link href="${coreCss}" rel="stylesheet"/>
    <link href="${jQueryUiCss}" rel="stylesheet"/>

    <script src="${jQueryJS}"></script>
    <script src="${jQueryUiJS}"></script>
    <script src="${environmentJS}"></script>
    <script src="${createDialogValidationJS}"></script>
    <script src="${createDialogJS}"></script>
    <script src="${uploadDialogJS}"></script>
    <script src="${mainJs}"></script>
</head>

<div class="header">
    <h3>Bank details</h3></p>
</div>

<hr>
<div class="container">
    <button class="popup-link-1" id="create-card-detail-open">Add new card detail</button>
    <button class="popup-link-2" id="upload-file-open">Upload file</button>
    <button class="popup-link-3" id="clear-all" href="<spring:url value="/clearAll"/>">Clear all cards</button>

    <c:if test="${not empty error}">
        <p class="error-msg">${error}</p>
    </c:if>

    <table style="width: 100%">
        <tr class="width-full">
            <th>Bank</th>
            <th>BankCard number</th>
            <th>Expiry date</th>
        </tr>

        <c:choose>
            <c:when test="${empty cards}">
                <tr class="width-full">
                    <td colspan="3">
                        <p>There is no one card detail by now.</p>
                    </td>
                </tr>
            </c:when>
            <c:otherwise>
                <c:forEach var="card" items="${cards}">
                    <tr>
                        <td>${card.bank}</td>
                        <td>${card.numberMasked}</td>
                        <td>
                            <fmt:formatDate value="${card.expiryDate}" var="formattedDate" type="date"
                                            pattern="MMM-yyyy"/>
                                ${formattedDate}
                        </td>
                    </tr>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </table>

    <div id="dialog-form" title="Adding new card detail" class="dialog hidden">
        <p class="validateTips">All form fields are required.</p>

        <spring:url value="/addCardDetail" var="createURL"/>
        <form:form id="createNewCardDetail" action="${createURL}" modelAttribute="cardTO" method="post">
            <fieldset>
                <table class="table width-full">
                    <tr>
                        <td nowrap="nowrap">
                            <form:label path="bankName">Bank name</form:label>
                        </td>
                        <td>
                            <form:input type="text" id="bankName" path="bankName" name="bankName"
                                        placeholder="Bank name"
                                        class="text ui-widget-content ui-corner-all width-full"/>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            <form:label path="cardNumber">Card number</form:label>
                        </td>
                        <td>
                            <form:input type="text" id="cardNumber" path="cardNumber" name="cardNumber"
                                        placeholder="xxxx-xxxx-xxxx-xxxx"
                                        class="text ui-widget-content ui-corner-all width-full"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="vertical-align-top" nowrap="nowrap">
                            <label>Expiry date</label>
                        </td>
                        <td>
                            <table class="width-full">
                                <tr>
                                    <td>
                                        <form:label path="expiryMonth">month</form:label>
                                    </td>
                                    <td>
                                        <form:input id="expiryMonth" path="expiryMonth" name="expiryMonth"
                                                    list="monthList"
                                                    placeholder="Choose any" class="width-full"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form:label path="expiryYear">year</form:label>
                                    </td>
                                    <td>
                                        <form:input type="number" id="expiryYear" path="expiryYear" name="expiryYear"
                                                    value="2015" width="50px" class="width-full"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br/>
                <datalist id="monthList">
                    <option value="Jan">January</option>
                    <option value="Feb">February</option>
                    <option value="Mar">March</option>
                    <option value="Apr">April</option>
                    <option value="May">May</option>
                    <option value="Jun">June</option>
                    <option value="Jul">July</option>
                    <option value="Aug">August</option>
                    <option value="Sep">September</option>
                    <option value="Oct">October</option>
                    <option value="Nov">November</option>
                    <option value="Dec">December</option>
                </datalist>

                <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
            </fieldset>
        </form:form>
    </div>

    <div id="upload-file-container" title="Uploading CSV card details file" class="dialog hidden">
        <form id="upload-form" enctype="multipart/form-data">
            <input type="file" name="file" required/>
        </form>
        <progress class="width-full"></progress>
    </div>

    <hr>
</div>

</body>
</html>
