package ru.demo.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.demo.model.Card;

import java.io.InputStream;
import java.util.List;

/**
 * Created by dmitri
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/spring-web-servlet-test.xml"})
public class InputDocumentParserImplTest {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private InputDocumentParser inputDocumentParser;

    @Test
    public void testParse() throws Exception {
        String withQuotesPath = "input.with.quotes.csv";
        String withoutQuotesPath = "input.without.quotes.csv";

        InputStream stream = getClass().getClassLoader().getResourceAsStream(withQuotesPath);
        List<Card> list1 = inputDocumentParser.parse(stream);
        Assert.assertNotNull(String.format("Parsing input csv file '%s'.", withQuotesPath), list1);
        int parsed = list1.size();
        logger.info("Processed '{}' file, parsed '{}' lines.", withQuotesPath, parsed);

        stream = getClass().getClassLoader().getResourceAsStream(withoutQuotesPath);
        List<Card> list2 = inputDocumentParser.parse(stream);
        Assert.assertNotNull(String.format("Parsing input csv file '%s'.", withoutQuotesPath), list2);
        Assert.assertEquals("The parsed cards in both files should be equal.", list1, list2);
        logger.info("Processed '{}' file, parsed '{}' lines.", withoutQuotesPath, parsed);
    }
}