package ru.demo.managers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.demo.model.Card;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static junit.framework.Assert.*;

/**
 * Created by dmitri
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/spring-web-servlet-test.xml"})
public class CardManagerTest {
    @Autowired
    private CardManager cardManager;

    private void assertFail(String[] input, String errorMsg) {
        Card card = cardManager.createCard(input);
        assertNull(errorMsg, card);
    }

    private void validateCardNumber(String[] input, String expectedCardNumber) throws ParseException {
        Card card = cardManager.createCard(input);
        assertNotNull(String.format("The card instance should be created based on the " +
                "following input: %s", input), card);
        assertEquals("The card number validation.", expectedCardNumber, card.getNumber());
    }

    private void validate(String[] input, String expectedBankName, String expectedCardNumber, String expectedExpiryDate, int expectedExpiryDateMonth, int expectedExpiryDateYear) throws ParseException {
        Card card = cardManager.createCard(input);
        assertNotNull(String.format("The card instance should be created based on the " +
                "following input: %s", input), card);
        assertEquals("The card number validation.", expectedCardNumber, card.getNumber());
        assertEquals("The bank name validation.", expectedBankName, card.getBank());
        assertEquals("The expiry date validation.", new SimpleDateFormat("dd.MM.yyyy").parse(expectedExpiryDate), card.getExpiryDate());
        Calendar cal = Calendar.getInstance();
        cal.setTime(card.getExpiryDate());
        assertEquals("The expiry date month validation.", expectedExpiryDateMonth, cal.get(Calendar.MONTH));
        assertEquals("The expiry date year validation.", expectedExpiryDateYear, cal.get(Calendar.YEAR));
    }

    @Test
    public void testCreateCard() throws Exception {
        /* Testing the empty input */
        assertFail(new String[]{"", "", ""}, "The result must be 'null' on the empty input line.");
        /* Testing the incorrect format (less than 3 parts) input */
        String[] input = new String[]{"5610-0000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-0000-0001"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        /* Testing the incorrect format (more than 3 parts) input */
        input = new String[]{"HSBC Canada", "5610-0000-0000-0001", "Nov-2017", "123"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /*
         * Testing the incorrect 1st input part
         */
        /* Testing the empty 1st input part */
        input = new String[]{"", "5610-0000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /*
         * Testing the incorrect 2nd input part
         */
        /* Testing the incorrect 2nd input part (empty one) */
        /* Testing the incorrect 2nd input part (empty one) */
        input = new String[]{"HSBC Canada", "", "Nov-2017;123"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "---", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        /* Testing the incorrect 2nd input part (no dash separator) */
        input = new String[]{"HSBC Canada", "5610000000000001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "56100000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-00000000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-00000001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        /* Testing the incorrect 2nd input part (no two dash separators) */
        input = new String[]{"HSBC Canada", "56100000-00000001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-000000000001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "561000000000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /* Testing the incorrect 2nd input part (extra dash separator) */
        input = new String[]{"HSBC Canada", "-5610-0000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610--0000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000--0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-0000--0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-0000-0001-", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /* Testing the incorrect 2nd input part (wrong amount of digits in the 1st group of card number) */
        input = new String[]{"HSBC Canada", "-0000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5-0000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "56100-0000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /* Testing the incorrect 2nd input part (wrong amount of digits in the 2nd group of card number) */
        input = new String[]{"HSBC Canada", "5610--0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-00000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /* Testing the incorrect 2nd input part (wrong amount of digits in the 3rd group of card number) */
        input = new String[]{"HSBC Canada", "5610-0000--0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-00000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /* Testing the incorrect 2nd input part (wrong amount of digits in the 4th group of card number) */
        input = new String[]{"HSBC Canada", "5610-0000-0000-", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-0000-00010", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /* Testing the incorrect 2nd input part (non digit symbol) */
        input = new String[]{"HSBC Canada", "56a10-0000-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-00a00-0000-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-00a00-0001", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        input = new String[]{"HSBC Canada", "5610-0000-0000-00a01", "Nov-2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /*
         * Testing the incorrect 3rd input part
         */
        /* Testing the incorrect 3rd input part (invalid format) */
        input = new String[]{"HSBC Canada", "5610-0000-0000-0001", "Nov/2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));
        /* Testing the incorrect 3rd input part (different date format) */
        input = new String[]{"HSBC Canada", "5610-0000-0000-0001", "01.11.2017"};
        assertFail(input, String.format("The result must be 'null' on the input line '%s'.", input));

        /* Testing the all three input part surrounded with the spaces*/
        input = new String[]{" HSBC Canada ", " 5610-0000-0000-0001 ", " Nov-2017 "};
        String expectedBankName = "HSBC Canada";
        String expectedCardNumber = "5610-0000-0000-0001";
        String expectedExpiryDate = "01.11.2017";
        int expectedExpiryDateMonth = Calendar.NOVEMBER;
        int expectedExpiryDateYear = 2017;
        validate(input, expectedBankName, expectedCardNumber, expectedExpiryDate, expectedExpiryDateMonth, expectedExpiryDateYear);

        /* Testing the valid (from given example #1) input */
        input = new String[]{"HSBC Canada", "5601-2345-3446-5678", "Nov-2017"};
        expectedBankName = "HSBC Canada";
        expectedCardNumber = "5601-2345-3446-5678";
        expectedExpiryDate = "01.11.2017";
        expectedExpiryDateMonth = Calendar.NOVEMBER;
        expectedExpiryDateYear = 2017;
        validate(input, expectedBankName, expectedCardNumber, expectedExpiryDate, expectedExpiryDateMonth, expectedExpiryDateYear);
        /* Testing the valid (from given example #2) input */
        input = new String[]{"Royal Bank of  Canada", "4519-4532-4524-2456", "Oct-2017"};
        expectedBankName = "Royal Bank of  Canada";
        expectedCardNumber = "4519-4532-4524-2456";
        expectedExpiryDate = "01.10.2017";
        expectedExpiryDateMonth = Calendar.OCTOBER;
        expectedExpiryDateYear = 2017;
        validate(input, expectedBankName, expectedCardNumber, expectedExpiryDate, expectedExpiryDateMonth, expectedExpiryDateYear);
        /* Testing the valid (from given example #3 - 15 digits in card number) input */
        input = new String[]{"American Express", "3786-7334-8965-345", "Dec-2018"};
        expectedBankName = "American Express";
        expectedCardNumber = "3786-7334-8965-345";
        expectedExpiryDate = "01.12.2018";
        expectedExpiryDateMonth = Calendar.DECEMBER;
        expectedExpiryDateYear = 2018;
        validate(input, expectedBankName, expectedCardNumber, expectedExpiryDate, expectedExpiryDateMonth, expectedExpiryDateYear);
        /* Testing the valid input (12 digits in card number) */
        input = new String[]{"American Express", "3786-7334-8965", "Dec-2018"};
        expectedCardNumber = "3786-7334-8965";
        validateCardNumber(input, expectedCardNumber);
        /* Testing the valid input (13 digits in card number) */
        input = new String[]{"American Express", "3786-7334-8965-3", "Dec-2018"};
        expectedCardNumber = "3786-7334-8965-3";
        validateCardNumber(input, expectedCardNumber);
        /* Testing the valid input (14 digits in card number) */
        input = new String[]{"American Express", "3786-7334-8965-34", "Dec-2018"};
        expectedCardNumber = "3786-7334-8965-34";
        validateCardNumber(input, expectedCardNumber);
        /* Testing the valid input (17 digits in card number) */
        input = new String[]{"American Express", "3786-7334-8965-3412-1", "Dec-2018"};
        expectedCardNumber = "3786-7334-8965-3412-1";
        validateCardNumber(input, expectedCardNumber);
        /* Testing the valid input (18 digits in card number) */
        input = new String[]{"American Express", "3786-7334-8965-3412-12", "Dec-2018"};
        expectedCardNumber = "3786-7334-8965-3412-12";
        validateCardNumber(input, expectedCardNumber);
        /* Testing the valid input (19 digits in card number) */
        input = new String[]{"American Express", "3786-7334-8965-3412-123", "Dec-2018"};
        expectedCardNumber = "3786-7334-8965-3412-123";
        validateCardNumber(input, expectedCardNumber);
    }
}