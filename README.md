# README #

This demo was made to show the simplest usage of Spring Web with Java and running it with [jetty](http://www.eclipse.org/jetty/)/[Apache Tomcat](http://tomcat.apache.org/).
To run it you just need to download it to your computer, have the Internet connection.

### What is this repository for? ###

**Quick summary**

This web application outputs bank card data based on the criteria provided below:
- sort the data by Expiry date in descending order;
- replace card number after the first 4 digits with ‘x’, e.g. 5601-2345-3446-5678 becomes 5601-xxxx-xxxx-xxxx
The end user is able to enter the card data manually, one row at a time, or upload a CSV file with the columns in the order shown above.
Following validation rules applied to each of input part:

  Input|Description
  -----|---------------------
  **Bank Name**| The string with the Bank name *(may contain even the delimiter - comma)*
 **Card Number** | The bank card number in the format XXXX-XXXX-...XX; may vary **from 12 to 19 digits** grouped by four digits (e.g. from 1234-5678-9012 to 1234-5678-9012-3456-789)
 **Expiry Date** | Bank card expiry date starting **from 1970 till 2030**

Inputted data stored only for the duration of the user’s session.
The results presented on a web page.
The output is like this (on the view original card number tail digits are replaced with 'x'):

  Bank|Card number|Expiry date
  ----|-----------|-----------
  HSBC Canada|5601-xxxx-xxxx-xxxx|Nov-2017
  Royal Bank of Canada|4519-xxxx-xxxx-xxxx|Oct-2017
  American Express|3786--xxxx-xxxx-xxx|Dec-2018

**Version**
Version 1.0


### How do I get set up? ###

**Summary of set up**

Download the project to your computer (assume, that it is <project_path>).

**Configuration**

You need to setup your JDK [1.7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html) or [later](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
and Maven [2.2.1](http://archive.apache.org/dist/maven/maven-2/2.2.1/) or [later](http://maven.apache.org/download.cgi) on your computer.
And make the project:
> mvn clean package 

**Dependencies**

All dependencies are setup through [Maven](https://maven.apache.org/), so you just need the valid Internet connection once running it for a first time.

**Database configuration**

Right now no DB is used in the project. That's why there are some restrictions:
- limit of input CSV-file is 50MB;
- there is no 
Possible future use of in-memory DB like [Memchached](http://memcached.org/) or [Redis](http://redis.io/).

**How to run tests**

Just as a Maven lifecycle phase:
> mvn test

  Now are available only unit tests. Functional and integration ones are planned only.
  For testing input CSV-files you may use any of files available for unit testing:
  > <project_path>/src/test/resources/input.with.quotes.csv

  > <project_path>/src/test/resources/input.without.quotes.csv

**Deployment instructions**

It is a stand-alone web application which can be deployed/run in one of the following two ways:

  1. directly from Maven using jetty (being in the <pjoect_path>)
  > mvn jetty:run

  2. packaged as a WAR file that we can deploy into an application container such as Tomcat. We will be using Java 7 to run the tests.

### Future plans ###

- Add in-memory DB to store the session scope bank card details for a user.
- Add a store (e.g. [Mongo’s GridFS](http://docs.mongodb.org/manual/core/gridfs/)) to use for a temporary storing the user input CSV-files. This may controls the file system usage and prevents from vulnerabilities such as uploading executables and overwriting existing files.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

    [Dmitri](http://linkedin.com/in/dmitriimelnik). 8-year experienced full stack Java developer with deep knowledge of UNIX systems and understanding of commercial development process principles.